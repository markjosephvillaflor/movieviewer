package by.anatoldeveloper.hallscheme.hall;

/**
 * Created by Nublo on 12.11.2015.
 * Copyright Nublo
 */
public interface SeatListener {

    void selectSeat(String id);
    void unSelectSeat(String id);

}