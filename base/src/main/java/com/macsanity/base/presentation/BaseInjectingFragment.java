package com.macsanity.base.presentation;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.CallSuper;
import android.support.annotation.LayoutRes;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.macsanity.base.R;

public abstract class BaseInjectingFragment extends Fragment {

    @Override
    public void onAttach(final Context context) {
        onInject();
        super.onAttach(context);
    }

    @Override
    @CallSuper
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(getLayoutId(), container, false);
    }

    public abstract void onInject();

    @LayoutRes
    protected abstract int getLayoutId();

}
