package com.macsanity.movieviewer.main.movie.data;

import javax.inject.Inject;

import io.reactivex.functions.Function;

/**
 * Created by Mark Joseph Villaflor on 2/9/18.
 */

public class CinemaDraftMapper implements Function<CinemaListDraftRaw, CinemaListDraftRaw> {

    @Inject
    public CinemaDraftMapper() {
    }

    @Override
    public CinemaListDraftRaw apply(CinemaListDraftRaw cinemaListDraft) throws Exception {
        return null;
    }
}
