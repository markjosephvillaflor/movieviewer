package com.macsanity.movieviewer.main.movie.data;

import android.support.annotation.NonNull;

import com.google.auto.value.AutoValue;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Mark Joseph Villaflor on 2/7/18.
 */

@AutoValue
public abstract class AvailableSeatDraft {

    @NonNull
    public abstract List<String> seats();

    public abstract int seatCount();

    @NonNull
    public static Builder builder(){
      return new AutoValue_AvailableSeatDraft.Builder();
    }

    @AutoValue.Builder
    interface Builder {
        Builder seats(@NonNull final List<String> seats);

        Builder seatCount(@NonNull final int count);

        AvailableSeatDraft build();
    }

}
