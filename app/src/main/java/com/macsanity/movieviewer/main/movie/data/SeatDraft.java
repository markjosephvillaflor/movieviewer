package com.macsanity.movieviewer.main.movie.data;

import android.support.annotation.NonNull;

import com.google.auto.value.AutoValue;

import java.util.List;

import polanski.option.Option;

/**
 * Created by Mark Joseph Villaflor on 2/7/18.
 */

@AutoValue
public abstract class SeatDraft {

    @NonNull
    public abstract List<List<String>> seatmap();

    @NonNull
    public abstract Option<AvailableSeatDraft> available();

    public abstract List<String> seatAvailable();

    @NonNull
    public static Builder builder() {
        return new AutoValue_SeatDraft.Builder();
    }

    @AutoValue.Builder
    interface Builder {

        Builder seatmap(final List<List<String>> seatMap);

        Builder available(final Option<AvailableSeatDraft> availableSeatDraft);

        Builder seatAvailable(final List<String> seatMap);

        SeatDraft build();

    }
}
