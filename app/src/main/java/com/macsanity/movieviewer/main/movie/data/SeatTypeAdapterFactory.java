package com.macsanity.movieviewer.main.movie.data;

import android.support.annotation.NonNull;

import com.google.gson.TypeAdapter;
import com.google.gson.TypeAdapterFactory;
import com.ryanharter.auto.value.gson.GsonTypeAdapterFactory;

/**
 * Created by Mark Joseph Villaflor on 2/7/18.
 */

@GsonTypeAdapterFactory
public abstract class SeatTypeAdapterFactory implements TypeAdapterFactory {

    @NonNull
    public static TypeAdapterFactory create() {
        return new AutoValueGson_SeatTypeAdapterFactory();
    }
}
