package com.macsanity.movieviewer.main.movie.data;

import android.support.annotation.NonNull;

import com.google.auto.value.AutoValue;
import com.google.gson.Gson;
import com.google.gson.TypeAdapter;
import com.google.gson.TypeAdapterFactory;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Mark Joseph Villaflor on 2/7/18.
 */

@AutoValue
public abstract class AvailableSeatDraftRaw {

    @SerializedName("seats")
    public abstract List<String> seats();

    @SerializedName("seat_count")
    public abstract int seatCount();

    @NonNull
    public static TypeAdapter<AvailableSeatDraftRaw> typeAdapterFactory(@NonNull final Gson gson) {
        return new AutoValue_AvailableSeatDraftRaw.GsonTypeAdapter(gson);
    }

    @NonNull
    public static Builder builder() {
        return new AutoValue_AvailableSeatDraftRaw.Builder();
    }

    @AutoValue.Builder
    interface Builder {
        Builder seats(@NonNull final List<String> seats);

        Builder seatCount(@NonNull final int count);

        AvailableSeatDraftRaw build();
    }
}
