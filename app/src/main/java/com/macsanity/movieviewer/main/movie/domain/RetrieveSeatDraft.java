package com.macsanity.movieviewer.main.movie.domain;

import android.support.annotation.NonNull;

import com.macsanity.movieviewer.main.movie.data.CinemaListDraft;
import com.macsanity.movieviewer.main.movie.data.CinemaListDraftRaw;
import com.macsanity.movieviewer.main.movie.data.SeatDraft;
import com.macsanity.movieviewer.main.movie.data.SeatRepository;

import javax.inject.Inject;

import io.reactivex.Flowable;

/**
 * Created by Mark Joseph Villaflor on 2/7/18.
 */

public class RetrieveSeatDraft {

    @NonNull
    private final SeatRepository seatRepository;

    @Inject
    public RetrieveSeatDraft(@NonNull SeatRepository seatRepository) {
        this.seatRepository = seatRepository;
    }

    @NonNull
    public Flowable<SeatDraft> getSeats() {
        return seatRepository.fetchSeatDrafts();
    }

    @NonNull
    public Flowable<CinemaListDraftRaw> getSchedules() {
        return seatRepository.fetchScheduleList();
    }
}
