package com.macsanity.movieviewer.main.movie.data;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.google.auto.value.AutoValue;
import com.google.gson.Gson;
import com.google.gson.TypeAdapter;
import com.google.gson.annotations.SerializedName;

import java.util.List;

import javax.annotation.Nonnull;

/**
 * Created by Mark Joseph Villaflor on 2/6/18.
 */

@AutoValue
public abstract class MovieDraftRaw {

    @Nullable
    @SerializedName("movie_id")
    abstract String movieId();

    @Nullable
    @SerializedName("advisory_rating")
    abstract String advisoryRating();

    @Nullable
    @SerializedName("canonical_title")
    abstract String canonicalTitle();

    @Nullable
    @SerializedName("cast")
    abstract List<String> cast();

    @Nullable
    @SerializedName("genre")
    abstract String genre();

    @SerializedName("has_schedules")
    abstract int hasSchedules();

    @SerializedName("is_inactive")
    abstract int isInactive();

    @SerializedName("is_showing")
    abstract int isShowing();

    @Nullable
    @SerializedName("link_name")
    abstract String linkName();

    @Nullable
    @SerializedName("poster")
    abstract String poster();

    @Nullable
    @SerializedName("poster_landscape")
    abstract String posterLandscape();

    @Nullable
    @SerializedName("release_date")
    abstract String releaseDate();

    @Nullable
    @SerializedName("runtime_mins")
    abstract String runtimeMins();

    @Nullable
    @SerializedName("synopsis")
    abstract String synopsis();

    @Nullable
    @SerializedName("trailer")
    abstract String trailer();

    @SerializedName("average_rating")
    abstract int averageRating();

    @SerializedName("total_reviews")
    abstract int totalReviews();

    @Nullable
    @SerializedName("variants")
    abstract List<String> variants();

    @Nullable
    @SerializedName("theater")
    abstract String theater();

    @SerializedName("order")
    abstract int order();

    @SerializedName("is_featured")
    abstract int isFeatured();

    @SerializedName("watch_list")
    abstract boolean watchList();

    @SerializedName("your_rating")
    abstract int yourRating();

    @NonNull
    public static TypeAdapter<MovieDraftRaw> typeAdapter(@NonNull final Gson gson){
        return new AutoValue_MovieDraftRaw.GsonTypeAdapter(gson);
    }

    @NonNull
    public static Builder builder() {
        return new AutoValue_MovieDraftRaw.Builder();
    }

    @AutoValue.Builder
    interface Builder {

        Builder movieId(final String movieId);

        Builder advisoryRating(final String advisoryRating);

        Builder canonicalTitle(final String canonicalTitle);

        Builder cast(final List<String> cast);

        Builder genre(final String genre);

        Builder hasSchedules(final int hasSchedules);

        Builder isInactive(final int isInactive);

        Builder isShowing(final int isShowing);

        Builder linkName(final String linkName);

        Builder poster(final String poster);

        Builder posterLandscape(final String posterLandscape);

        Builder releaseDate(final String releaseDate);

        Builder runtimeMins(final String runtimeMins);

        Builder synopsis(final String synopsis);

        Builder trailer(final String trailer);

        Builder averageRating(final int averageRating);

        Builder totalReviews(final int totalReviews);

        Builder variants(final List<String> variants);

        Builder theater(final String theater);

        Builder order(final int order);

        Builder isFeatured(final int isFeatured);

        Builder watchList(final boolean watchList);

        Builder yourRating(final int watchList);

        MovieDraftRaw build();
    }
}
