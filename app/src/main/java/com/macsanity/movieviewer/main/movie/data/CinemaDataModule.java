package com.macsanity.movieviewer.main.movie.data;

import com.google.gson.TypeAdapterFactory;

import dagger.Module;
import dagger.Provides;
import dagger.multibindings.IntoSet;

/**
 * Created by macsanity on 2/8/18.
 */

@Module
public class CinemaDataModule {
    @Provides
    @IntoSet
    TypeAdapterFactory provideTypeAdapterFactory() {
        return CinemaTypeAdapterFactory.create();
    }

}
