package com.macsanity.movieviewer.main;

import android.content.Context;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.SpinnerAdapter;
import android.widget.TextView;

import com.macsanity.movieviewer.main.movie.data.ChildCinemaTimeDraftRaw;
import com.macsanity.movieviewer.main.movie.data.CinemaTimeDraftRaw;

import java.util.List;

/**
 * Created by macsanity on 2/10/18.
 */

public class TimeAdapter extends BaseAdapter implements SpinnerAdapter {

    private final Context mContext;
    private List<ChildCinemaTimeDraftRaw> timeDraftRawList;

    public TimeAdapter(Context mContext) {
        this.mContext = mContext;
    }

    public void addData(List<ChildCinemaTimeDraftRaw> raws){
        this.timeDraftRawList = raws;
    }

    public void clearData(){
        timeDraftRawList.clear();
    }

    @Override
    public int getCount() {
        return timeDraftRawList.size();
    }

    @Override
    public Object getItem(int i) {
        return timeDraftRawList.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        TextView label = new TextView(mContext);
        label.setPadding(16, 16, 16, 16);
        label.setTextColor(Color.BLACK);
        label.setText(timeDraftRawList.get(position).label);

        return label;
    }

    @Override
    public View getDropDownView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {

        TextView label = new TextView(mContext);
        label.setPadding(16, 16, 16, 16);
        label.setTextColor(Color.BLACK);
        label.setText(timeDraftRawList.get(position).label);

        return label;
    }

}
