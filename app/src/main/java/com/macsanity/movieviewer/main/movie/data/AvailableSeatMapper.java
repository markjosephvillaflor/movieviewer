package com.macsanity.movieviewer.main.movie.data;

import android.support.annotation.NonNull;

import com.macsanity.base.data.EssentialParamMissingException;

/**
 * Created by Mark Joseph Villaflor on 2/7/18.
 */

final class AvailableSeatMapper {

    @NonNull
    @SuppressWarnings("ConstantConditions")
    static AvailableSeatDraft processRaw(@NonNull final AvailableSeatDraftRaw raw) {
        assertEssentialParams(raw);

        return AvailableSeatDraft.builder()
                .seats(raw.seats())
                .seatCount(raw.seatCount())
                .build();
    }

    private static void assertEssentialParams(@NonNull final AvailableSeatDraftRaw raw) {
        String missingParams = "";

        if (raw.seats() == null || raw.seats().isEmpty()) {
            missingParams += "seats";
        }

        if (!missingParams.isEmpty()) {
            throw new EssentialParamMissingException(missingParams, raw);
        }
    }
}
