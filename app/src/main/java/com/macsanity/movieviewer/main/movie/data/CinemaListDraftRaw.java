package com.macsanity.movieviewer.main.movie.data;

import android.support.annotation.NonNull;

import com.google.auto.value.AutoValue;
import com.google.gson.Gson;
import com.google.gson.TypeAdapter;
import com.google.gson.annotations.SerializedName;

import java.util.List;
import java.util.Map;

/**
 * Created by macsanity on 2/8/18.
 */
//TODO:: Apply sanity checking in Polymorphic objects
public class CinemaListDraftRaw {

    @NonNull
    @SerializedName("dates")
    public List<CinemaDateDraftRaw> dates;

    @NonNull
    @SerializedName("cinemas")
    public  List<CinemaDraftRaw> cinemaDraftRaws;

    @NonNull
    @SerializedName("times")
    public  List<CinemaTimeDraftRaw> cinemaTimeDraftRaws;

}
