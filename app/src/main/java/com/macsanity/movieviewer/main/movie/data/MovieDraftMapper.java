package com.macsanity.movieviewer.main.movie.data;


import android.support.annotation.NonNull;
import android.support.v4.util.Pair;
import android.text.format.DateUtils;
import android.util.Log;

import com.macsanity.base.common.utils.TimeUtils;
import com.macsanity.base.data.EssentialParamMissingException;

import javax.inject.Inject;

import io.reactivex.functions.Function;

/**
 * Created by macsanity on 2/6/18.
 */

public class MovieDraftMapper implements Function<MovieDraftRaw, MovieDraft> {

    public static final String TAG = MovieDraftMapper.class.getSimpleName();

    @NonNull
    private final TimeUtils timeUtils;

    @Inject
    public MovieDraftMapper(@NonNull final TimeUtils timeUtils) {
        this.timeUtils = timeUtils;
    }

    @Override
    public MovieDraft apply(@NonNull final MovieDraftRaw raw) throws Exception {
        assertEssentialParams(raw);
        return MovieDraft.builder()
                .movieId(raw.movieId())
                .canonicalTitle(raw.canonicalTitle())
                .genre(raw.genre())
                .poster(raw.poster())
                .posterLandscape(raw.posterLandscape())
                .cast(raw.cast())
                .advisoryRating(raw.advisoryRating())
                .runtimeMins(raw.runtimeMins())
                .releaseDate(mapToFormattedReleaseDate(raw.releaseDate()))
                .synopsis(raw.synopsis())
                .theater(raw.theater())
                .build();
    }

    @SuppressWarnings("unchecked")
    @NonNull
    private String mapToFormattedReleaseDate(@NonNull final String paidOutDate) {
        return timeUtils.formatDateString(paidOutDate, "MMM d, yyyy");
    }

    private void assertEssentialParams(@NonNull final MovieDraftRaw raw) {
        String missingParams = "";

        if (raw.movieId() == null || raw.movieId().isEmpty()) {
            missingParams += "id ";
        }

        if (raw.canonicalTitle() == null || raw.canonicalTitle().isEmpty()) {
            missingParams += "canonicalTitle ";
        }

        if (raw.genre() == null || raw.genre().isEmpty()) {
            missingParams += "genre ";
        }

        if (raw.advisoryRating() == null || raw.advisoryRating().isEmpty()) {
            missingParams += "advisoryRating ";
        }

        if (raw.runtimeMins() == null || raw.runtimeMins().isEmpty()) {
            missingParams += "runtimeMins ";
        }

        if (raw.releaseDate() == null || raw.releaseDate().isEmpty()) {
            missingParams += "releaseDate ";
        }

        if (raw.releaseDate() == null || raw.releaseDate().isEmpty()) {
            missingParams += "releaseDate ";
        }

        if (raw.synopsis() == null || raw.synopsis().isEmpty()) {
            missingParams += "synopsis ";
        }

        if (!missingParams.isEmpty()) {
            throw new EssentialParamMissingException(missingParams, raw);
        }

    }
}
