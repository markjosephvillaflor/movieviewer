package com.macsanity.movieviewer.main.movie.data;

import com.google.auto.value.AutoValue;
import com.google.gson.annotations.SerializedName;

/**
 * Created by macsanity on 2/8/18.
 */
@AutoValue
public abstract class ChildCinemaTimeDraft {

    abstract String id();

    abstract String label();

    abstract String scheduleId();

    abstract String popcornPrice();

    abstract String popcornLabel();

    abstract String seatingType();

    abstract String price();

    public static Builder builder(){
        return new AutoValue_ChildCinemaTimeDraft.Builder();
    }

    @AutoValue.Builder
    interface Builder {

        Builder id(final String id);
        Builder label(final String label);
        Builder scheduleId(final String scheduleId);
        Builder popcornPrice(final String popCornPrice);
        Builder popcornLabel(final String popCornLabel);
        Builder seatingType(final String seatingType);
        Builder price(final String price);

        ChildCinemaTimeDraft build();
    }
}
