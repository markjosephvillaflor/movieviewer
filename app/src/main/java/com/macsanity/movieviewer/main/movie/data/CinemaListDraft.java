package com.macsanity.movieviewer.main.movie.data;

import android.support.annotation.NonNull;

import com.google.auto.value.AutoValue;

import java.util.List;
import java.util.Map;

/**
 * Created by macsanity on 2/8/18.
 */

@AutoValue
public  abstract class CinemaListDraft {

    @NonNull
    public abstract List<CinemaDateDraft> dates();

    @NonNull
    public abstract Map<String, List<CinemaDraft>> cinemaDraft();

    @NonNull
    public abstract Map<String, List<CinemaTimeDraft>> cinemaTimeDraft();

    public static Builder builder(){
        return new AutoValue_CinemaListDraft.Builder();
    }

    @AutoValue.Builder
    interface Builder {

        Builder dates(final List<CinemaDateDraft> dates);
        Builder cinemaDraft(final Map<String, List<CinemaDraft>> cinema);
        Builder cinemaTimeDraft(final Map<String, List<CinemaTimeDraft>> time);

        CinemaListDraft build();
    }
}
