package com.macsanity.movieviewer.main.presentation;

import com.macsanity.base.injection.modules.ActivityModule;
import com.macsanity.base.injection.scopes.ActivityScope;
import com.macsanity.movieviewer.main.movie.presentation.seatmap.SeatMapActivity;
import com.macsanity.movieviewer.main.movie.presentation.viewer.MovieComponent;

import dagger.Subcomponent;

/**
 * Created by macsanity on 2/6/18.
 */

@ActivityScope
@Subcomponent(modules = {ActivityModule.class})
public interface MainActivityComponent {
    void inject(MainActivity mainActivity);
    void inject(SeatMapActivity seatMapActivity);
}
