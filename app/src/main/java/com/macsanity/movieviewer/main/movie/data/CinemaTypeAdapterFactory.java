package com.macsanity.movieviewer.main.movie.data;

import android.support.annotation.NonNull;

import com.google.gson.Gson;
import com.google.gson.TypeAdapter;
import com.google.gson.TypeAdapterFactory;
import com.google.gson.reflect.TypeToken;
import com.ryanharter.auto.value.gson.GsonTypeAdapterFactory;

/**
 * Created by macsanity on 2/8/18.
 */
@GsonTypeAdapterFactory
public abstract class CinemaTypeAdapterFactory implements TypeAdapterFactory {
    @NonNull
    public static TypeAdapterFactory create(){
        return new AutoValueGson_CinemaTypeAdapterFactory();
    }
}
