package com.macsanity.movieviewer.main.movie;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.macsanity.movieviewer.R;
import com.macsanity.movieviewer.main.movie.data.SeatSelectedDraft;

import java.util.List;

/**
 * Created by macsanity on 2/10/18.
 */

public class SeatAdapter extends RecyclerView.Adapter<SeatAdapter.ItemRowHolder> {

    private final Context context;
    private List<SeatSelectedDraft> dataList;

    public SeatAdapter(Context context) {
        this.context = context;
    }

    @Override
    public SeatAdapter.ItemRowHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_item, null);
        ItemRowHolder mh = new ItemRowHolder(v);
        return mh;
    }

    @Override
    public void onBindViewHolder(SeatAdapter.ItemRowHolder holder, int position) {
            holder.bind(dataList.get(position));
    }

    @Override
    public int getItemCount() {
        return (null != dataList ? dataList.size() : 0);
    }


    public void addData(List<SeatSelectedDraft> list) {
        this.dataList = list;
    }

    public void removeItem(int position){
        dataList.remove(position);
        notifyItemRemoved(position);
        notifyItemRangeChanged(position, getItemCount() - position);
    }

    class ItemRowHolder extends RecyclerView.ViewHolder {

        TextView txtSeatName;
        TextView txtSeatPrice;

        ItemRowHolder(View view) {
            super(view);
            txtSeatName = view.findViewById(R.id.tv_seat_name);
            txtSeatPrice = view.findViewById(R.id.tv_seat_price);
        }

        void bind(SeatSelectedDraft seatSelectedDraft){
            txtSeatName.setText(seatSelectedDraft.selectedSeat);
            txtSeatPrice.setText(seatSelectedDraft.selectedSeatPrice);
        }

    }
}
