package com.macsanity.movieviewer.main.movie.presentation.viewer;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.ViewModel;
import android.support.annotation.NonNull;
import android.util.Log;


import com.macsanity.movieviewer.main.movie.data.MovieDisplayableItemMapper;
import com.macsanity.movieviewer.main.movie.data.MovieDraft;
import com.macsanity.movieviewer.main.movie.domain.RetrieveMovieDraft;

import javax.inject.Inject;

import dagger.Module;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import polanski.option.Option;

/**
 * Created by Mark Joseph Villaflor on 2/6/18.
 */

public class MovieViewModel extends ViewModel {

    @NonNull
    private final RetrieveMovieDraft retrieveMovieDraft;

    @NonNull
    private final MovieDisplayableItemMapper movieDisplayableItemMapper;

    @NonNull
    private final MutableLiveData<MovieDraft> movieLiveData = new MutableLiveData<>();

    @NonNull
    private final CompositeDisposable compositeDisposable = new CompositeDisposable();

    @Inject
    public MovieViewModel(@NonNull RetrieveMovieDraft retrieveMovieDraft, @NonNull MovieDisplayableItemMapper movieDisplayableItemMapper) {
        this.retrieveMovieDraft = retrieveMovieDraft;
        this.movieDisplayableItemMapper = movieDisplayableItemMapper;

        compositeDisposable.add(addToMovieDrafts());
    }

    @Override
    protected void onCleared() {
        super.onCleared();
        compositeDisposable.dispose();
    }

    @NonNull
    public LiveData<MovieDraft> getMovieLiveData() {
        return movieLiveData;
    }

    private Disposable addToMovieDrafts() {
        return retrieveMovieDraft.getMovieDrafts()
                .observeOn(Schedulers.computation())
                .subscribe(movieLiveData::postValue, throwable -> Log.e("Error", "" + throwable.getMessage()));

    }
}
