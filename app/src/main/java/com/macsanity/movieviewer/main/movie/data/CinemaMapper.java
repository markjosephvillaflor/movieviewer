package com.macsanity.movieviewer.main.movie.data;

import android.support.annotation.NonNull;

import com.macsanity.base.data.EssentialParamMissingException;

import java.util.List;

/**
 * Created by macsanity on 2/8/18.
 */

final class CinemaMapper {

    @NonNull
    @SuppressWarnings("ConstantConditions")
    static CinemaDraft processRaw(@NonNull final CinemaDraftRaw raw) {
        assertEssentialParams(raw);

        return null;
    }

    private static void assertEssentialParams(@NonNull final CinemaDraftRaw raw) {
        String missingParams = "";

        if (raw.list == null || raw.list.isEmpty()) {
            missingParams += "cinema";
        }

        if (!missingParams.isEmpty()) {
            throw new EssentialParamMissingException(missingParams, raw);
        }
    }
}
