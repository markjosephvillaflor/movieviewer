package com.macsanity.movieviewer.main.movie.data;

import com.google.gson.TypeAdapterFactory;

import dagger.Module;
import dagger.Provides;
import dagger.multibindings.IntoSet;

/**
 * Created by Mark Joseph Villaflor on 2/7/18.
 */

@Module
public class SeatDataModule {

    @Provides
    @IntoSet
    TypeAdapterFactory provideTypeAdapterFactory() {
        return SeatTypeAdapterFactory.create();
    }


}
