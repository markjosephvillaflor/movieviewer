package com.macsanity.movieviewer.main.movie.data;

/**
 * Created by macsanity on 2/10/18.
 */

public class SeatSelectedDraft {

    public String selectedSeat;
    public String selectedSeatPrice;

    public SeatSelectedDraft(String selectedSeat, String selectedSeatPrice) {
        this.selectedSeat = selectedSeat;
        this.selectedSeatPrice = selectedSeatPrice;
    }
}
