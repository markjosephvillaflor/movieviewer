package com.macsanity.movieviewer.main.movie.data;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.google.auto.value.AutoValue;

import java.util.List;

/**
 * Created by Mark Joseph Villaflor on 2/6/18.
 */

@AutoValue
public abstract class MovieDraft {

    @Nullable
    public abstract String movieId();

    @Nullable
    public abstract String advisoryRating();

    @Nullable
    public  abstract String canonicalTitle();

    @Nullable
    public abstract List<String> cast();

    @Nullable
    public  abstract String genre();

    @Nullable
    public abstract String poster();

    @Nullable
    public abstract String posterLandscape();

    @Nullable
    public abstract String releaseDate();

    @Nullable
    public  abstract String runtimeMins();

    @Nullable
    public abstract String synopsis();

    @Nullable
    public abstract String theater();

    @NonNull
    public static Builder builder() {
        return new AutoValue_MovieDraft.Builder();
    }

    @AutoValue.Builder
    interface Builder {

        Builder movieId(String movieId);

        Builder advisoryRating(final String advisoryRating);

        Builder canonicalTitle(final String canonicalTitle);

        Builder cast(final List<String> cast);

        Builder genre(final String genre);

        Builder poster(final String poster);

        Builder posterLandscape(final String posterLandscape);

        Builder releaseDate(final String releaseDate);

        Builder runtimeMins(final String runtimeMins);

        Builder synopsis(final String synopsis);

        Builder theater(final String theater);

        MovieDraft build();
    }
}
