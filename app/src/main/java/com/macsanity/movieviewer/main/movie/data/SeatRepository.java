package com.macsanity.movieviewer.main.movie.data;

import android.support.annotation.NonNull;
import android.util.Log;

import javax.inject.Inject;
import javax.inject.Singleton;

import io.reactivex.BackpressureStrategy;
import io.reactivex.Flowable;
import io.reactivex.Observable;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by Mark Joseph Villaflor on 2/7/18.
 */

@Singleton
public class SeatRepository {

    @NonNull
    private final MovieService movieService;

    @NonNull
    private final SeatDraftMapper seatDraftMapper;

    @NonNull
    private final CinemaDraftMapper cinemaDraftMapper;

    @Inject
    public SeatRepository(@NonNull MovieService movieService, @NonNull SeatDraftMapper seatDraftMapper, @NonNull CinemaDraftMapper cinemaDraftMapper) {
        this.movieService = movieService;
        this.seatDraftMapper = seatDraftMapper;
        this.cinemaDraftMapper = cinemaDraftMapper;
    }

    @NonNull
    public Flowable<SeatDraft> fetchSeatDrafts() {
        return movieService.getSeat()
                .subscribeOn(Schedulers.io())
                .subscribeOn(Schedulers.computation())
                .flatMapObservable(it -> Observable.just(it.body()))
                .doOnError(throwable -> Log.e("fetchSeatDrafts", "Error -> ", throwable))
                .map(seatDraftMapper)
                .toFlowable(BackpressureStrategy.LATEST);
    }

    public Flowable<CinemaListDraftRaw> fetchScheduleList() {
        return movieService.getSchedule()
                .subscribeOn(Schedulers.io())
                .observeOn(Schedulers.computation())
                .flatMapObservable(it -> Observable.just(it.body()))
                .toFlowable(BackpressureStrategy.LATEST);
    }
}
