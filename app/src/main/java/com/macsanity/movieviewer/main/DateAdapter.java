package com.macsanity.movieviewer.main;

import android.content.Context;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.SpinnerAdapter;
import android.widget.TextView;

import com.macsanity.movieviewer.main.movie.data.CinemaDateDraft;
import com.macsanity.movieviewer.main.movie.data.CinemaDateDraftRaw;

import java.util.List;

/**
 * Created by macsanity on 2/10/18.
 */

public class DateAdapter extends BaseAdapter implements SpinnerAdapter {

    private Context mContext;
    private List<CinemaDateDraftRaw> mListCinema;


    public DateAdapter(@NonNull Context context) {
        this.mContext = context;
    }

    public void addData(List<CinemaDateDraftRaw> mList){
        this.mListCinema = mList;
    }
    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {

        TextView mTextView = new TextView(mContext);
        mTextView.setPadding(16, 16, 16, 16);
        mTextView.setTextColor(Color.BLACK);
        mTextView.setText(mListCinema.get(position).label());

        return mTextView;
    }


    @Override
    public View getDropDownView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {

        TextView label = new TextView(mContext);
        label.setPadding(16, 16, 16, 16);
        label.setTextColor(Color.BLACK);
        label.setText(mListCinema.get(position).label());

        return label;
    }

    @Override
    public int getCount() {
        return mListCinema.size();
    }

    @Override
    public CinemaDateDraftRaw getItem(int i) {
        return mListCinema.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }
}
