package com.macsanity.movieviewer.main.movie.data;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import javax.inject.Inject;

import io.reactivex.functions.Function;
import polanski.option.Option;

/**
 * Created by Mark Joseph Villaflor on 2/7/18.
 */

public class SeatDraftMapper implements Function<SeatDraftRaw, SeatDraft> {

    @Inject
    public SeatDraftMapper() {}

    @Override
    public SeatDraft apply(SeatDraftRaw raw) throws Exception {
        return SeatDraft.builder()
                .seatmap(raw.seatmap())
                .available(mapAvailableInfo(raw.available()))
                .seatAvailable(raw.available().seats())
                .build();
    }

    @NonNull
    private static Option<AvailableSeatDraft> mapAvailableInfo(@Nullable final AvailableSeatDraftRaw raw) {
        if (raw == null) {
            return Option.none();
        }

        return Option.ofObj(AvailableSeatMapper.processRaw(raw));
    }
}
