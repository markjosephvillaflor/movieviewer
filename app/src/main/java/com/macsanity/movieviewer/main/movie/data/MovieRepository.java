package com.macsanity.movieviewer.main.movie.data;

import android.support.annotation.NonNull;
import android.util.Log;

import com.macsanity.base.data.store.ReactiveStore;


import javax.inject.Inject;
import javax.inject.Singleton;

import io.reactivex.BackpressureStrategy;
import io.reactivex.Flowable;
import io.reactivex.Observable;
import io.reactivex.schedulers.Schedulers;
import polanski.option.Option;

/**
 * Created by macsanity on 2/6/18.
 */

@Singleton
public class MovieRepository {

    @NonNull
    private final MovieService movieService;

    @NonNull
    private final MovieDraftMapper movieDraftMapper;

    @Inject
    public MovieRepository(@NonNull MovieService movieService, @NonNull MovieDraftMapper movieDraftMapper) {
        this.movieService = movieService;
        this.movieDraftMapper = movieDraftMapper;
    }

    @NonNull
    public Flowable<MovieDraft> fetchSingleMovieDrafts() {
        return movieService.getMovie()
                .subscribeOn(Schedulers.io())
                .observeOn(Schedulers.computation())
                .flatMapObservable(it -> Observable.just(it.body()))
                // map from raw to safe
                .doOnError(throwable -> Log.e("fetchSingleMovieDrafts", "Error -> ", throwable))
                .map(movieDraftMapper)
                .toFlowable(BackpressureStrategy.LATEST);
    }
}
