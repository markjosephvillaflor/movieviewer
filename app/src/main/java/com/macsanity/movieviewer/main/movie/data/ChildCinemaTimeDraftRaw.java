package com.macsanity.movieviewer.main.movie.data;

import com.google.auto.value.AutoValue;
import com.google.gson.Gson;
import com.google.gson.TypeAdapter;
import com.google.gson.annotations.SerializedName;

/**
 * Created by macsanity on 2/8/18.
 */

public class ChildCinemaTimeDraftRaw {

    @SerializedName("id")
    public String id;

    @SerializedName("label")
    public String label;

    @SerializedName("schedule_id")
    public String scheduleId;

    @SerializedName("popcorn_price")
    public String popcornPrice;

    @SerializedName("popcorn_label")
    public String popcornLabel;

    @SerializedName("seating_type")
    public String seatingType;

    @SerializedName("price")
    public String price;


}
