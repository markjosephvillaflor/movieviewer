package com.macsanity.movieviewer.main.movie.presentation.viewer;

import com.macsanity.base.injection.scopes.FragmentScope;
import com.macsanity.movieviewer.main.movie.presentation.seatmap.SeatMapActivity;


import dagger.Subcomponent;

/**
 * Created by macsanity on 2/6/18.
 */

@FragmentScope
@Subcomponent
public interface MovieComponent {

    void inject(MovieViewerFragment movieViewerFragment);
    void inject(SeatMapActivity seatMapActivity);

    interface MovieComponentCreator {
        MovieComponent createMovieComponent();
    }
}
