package com.macsanity.movieviewer.main.movie.data;

import android.support.annotation.NonNull;

import com.google.auto.value.AutoValue;
import com.google.gson.Gson;
import com.google.gson.TypeAdapter;
import com.google.gson.annotations.SerializedName;

/**
 * Created by macsanity on 2/8/18.
 */

@AutoValue
public abstract class CinemaDateDraftRaw {

    @NonNull
    @SerializedName("id")
    public abstract String id();

    @NonNull
    @SerializedName("label")
    public abstract String label();

    @NonNull
    @SerializedName("date")
    abstract String date();

    public static Builder builder(){
        return new AutoValue_CinemaDateDraftRaw.Builder();
    }

    public static TypeAdapter<CinemaDateDraftRaw> typeAdapter(final Gson gson){
        return new AutoValue_CinemaDateDraftRaw.GsonTypeAdapter(gson);
    }
    @AutoValue.Builder
    interface Builder {

        Builder id(final String id);
        Builder label(final String label);
        Builder date(final String date);

        CinemaDateDraftRaw build();
    }

    @Override
    public String toString() {
        return label();
    }
}
