package com.macsanity.movieviewer.main.movie.presentation.viewer;


import android.arch.lifecycle.ViewModelProvider;
import android.arch.lifecycle.ViewModelProviders;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.AppCompatTextView;
import android.util.Log;
import android.view.View;

import com.macsanity.base.presentation.BaseInjectingActivity;
import com.macsanity.base.presentation.BaseInjectingFragment;
import com.macsanity.movieviewer.R;
import com.macsanity.movieviewer.main.movie.data.MovieDraft;
import com.squareup.picasso.Picasso;

import javax.inject.Inject;
import javax.inject.Named;

public class MovieViewerFragment extends BaseInjectingFragment implements View.OnClickListener {


    private AppCompatTextView mTxtTitle;
    private AppCompatTextView mTxtAdvisory;
    private AppCompatTextView mTxtRuntime;
    private AppCompatTextView mTxtGenre;
    private AppCompatTextView mTxtReleaseDate;
    private AppCompatTextView mTxtSypnosis;

    private AppCompatImageView mImgPoster;
    private AppCompatImageView mImgLandscapePoster;

    public MovieViewerFragment() {}

    @Override
    public void onInject() {
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle("Movie Viewer");
        ((AppCompatActivity) getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        mTxtTitle = view.findViewById(R.id.text_movie_title);
        mTxtAdvisory = view.findViewById(R.id.text_advisory_rating);
        mTxtRuntime = view.findViewById(R.id.text_runtime);
        mTxtGenre = view.findViewById(R.id.text_genre);
        mTxtReleaseDate = view.findViewById(R.id.text_release_date);
        mTxtSypnosis = view.findViewById(R.id.text_synopsis);

        mImgPoster = view.findViewById(R.id.image_poster_potrait);
        mImgLandscapePoster = view.findViewById(R.id.image_poster_landscape);

        AppCompatButton mBtnSeatMap = view.findViewById(R.id.button_view_seatmap);
        mBtnSeatMap.setOnClickListener(this);
    }

    @Override
    protected int getLayoutId() {
        return R.layout.fragment_movie_viewer;
    }

    private void setData(MovieDraft item) {
        Log.d("setData", item.poster());

        mTxtTitle.setText(item.canonicalTitle());
        mTxtAdvisory.setText(item.advisoryRating());
        mTxtRuntime.setText(item.runtimeMins());
        mTxtGenre.setText(item.genre());
        mTxtReleaseDate.setText(item.releaseDate());
        mTxtSypnosis.setText(item.synopsis());

        Picasso.with(getActivity()).load(item.poster()).into(mImgPoster);
        Picasso.with(getActivity()).load(item.posterLandscape()).into(mImgLandscapePoster);

    }

    @Override
    public void onClick(View view) {

    }
}
