package com.macsanity.movieviewer.main.movie.data;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.ViewModel;
import android.support.annotation.NonNull;
import android.util.Log;


import com.macsanity.movieviewer.main.movie.domain.RetrieveSeatDraft;

import javax.inject.Inject;

import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by Mark Joseph Villaflor on 2/7/18.
 */

public class SeatViewModel extends ViewModel {

    @NonNull
    private final RetrieveSeatDraft retrieveSeatDraft;

    @NonNull
    private final CompositeDisposable compositeDisposable = new CompositeDisposable();

    @NonNull
    private final MutableLiveData<SeatDraft> seatLiveData = new MutableLiveData<>();

    @NonNull
    private final MutableLiveData<CinemaListDraftRaw> scheduleLiveData = new MutableLiveData<>();

    @Inject
    public SeatViewModel(@NonNull RetrieveSeatDraft retrieveSeatDraft) {
        this.retrieveSeatDraft = retrieveSeatDraft;

        compositeDisposable.add(addToSeatDrafts());
        compositeDisposable.add(addToScheduleDrafts());
    }

    @Override
    protected void onCleared() {
        super.onCleared();
        compositeDisposable.dispose();
    }


    @NonNull
    public LiveData<SeatDraft> getSeatLiveData() {
        return seatLiveData;
    }

    public LiveData<CinemaListDraftRaw> getScheduleLiveData() {
        return scheduleLiveData;
    }


    private Disposable addToSeatDrafts() {
        return retrieveSeatDraft.getSeats().observeOn(Schedulers.computation())
                .subscribe(seatLiveData::postValue, throwable -> Log.e("Error", "" + throwable.getMessage()));
    }

    private Disposable addToScheduleDrafts() {
        return retrieveSeatDraft.getSchedules().observeOn(Schedulers.computation())
                .subscribe(scheduleLiveData::postValue, throwable -> Log.e("Error", "" + throwable.getLocalizedMessage()));
    }

}
