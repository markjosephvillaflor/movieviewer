package com.macsanity.movieviewer.main.movie.data;

import io.reactivex.Single;
import retrofit2.Response;
import retrofit2.http.GET;

/**
 * Created by Mark Joseph Villaflor on 2/6/18.
 */

public interface MovieService {

    @GET("movie.json")
    Single<Response<MovieDraftRaw>> getMovie();

    @GET("seatmap.json")
    Single<Response<SeatDraftRaw>> getSeat();

    @GET("schedule.json")
    Single<Response<CinemaListDraftRaw>> getSchedule();
}
