package com.macsanity.movieviewer.main.movie.domain;

import android.support.annotation.NonNull;


import com.macsanity.movieviewer.main.movie.data.MovieDraft;
import com.macsanity.movieviewer.main.movie.data.MovieRepository;

import javax.inject.Inject;

import io.reactivex.Flowable;

/**
 * Created by Mark Joseph Villaflor on 2/6/18.
 */

public class RetrieveMovieDraft {

    @NonNull
    private final MovieRepository movieRepository;

    @Inject
    public RetrieveMovieDraft(@NonNull MovieRepository movieRepository) {
        this.movieRepository = movieRepository;
    }

    @NonNull
    public Flowable<MovieDraft> getMovieDrafts() {
        return movieRepository.fetchSingleMovieDrafts();
    }
}
