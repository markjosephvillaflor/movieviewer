package com.macsanity.movieviewer.main.movie.data;

import android.support.annotation.NonNull;

import com.google.auto.value.AutoValue;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by macsanity on 2/8/18.
 */


public class CinemaDraftRaw {

    @NonNull
    @SerializedName("parent")
    public String parent;

    @NonNull
    @SerializedName("cinemas")
    public List<ChildCinemaDraftRaw> list;

}
