package com.macsanity.movieviewer.main.movie.data;

import android.support.annotation.NonNull;

import com.google.auto.value.AutoValue;
import com.google.gson.annotations.SerializedName;

/**
 * Created by macsanity on 2/8/18.
 */

@AutoValue
public abstract class CinemaDateDraft {

    @NonNull
    public abstract String id();

    @NonNull
    public abstract String label();

    @NonNull
    public abstract String date();

    public static Builder builder(){
        return new AutoValue_CinemaDateDraft.Builder();
    }

    @AutoValue.Builder
    interface Builder {
        Builder id(final String id);
        Builder label(final String label);
        Builder date(final String date);

        CinemaDateDraft build();
    }

}
