package com.macsanity.movieviewer.main.movie.data;

import com.google.gson.TypeAdapterFactory;
import com.macsanity.base.common.providers.TimestampProvider;
import com.macsanity.base.data.cache.Cache;
import com.macsanity.base.data.store.MemoryReactiveStore;
import com.macsanity.base.data.store.ReactiveStore;
import com.macsanity.base.data.store.Store;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import dagger.multibindings.IntoSet;

/**
 * Created by Mark Joseph Villaflor on 2/6/18.
 */

@Module
public class MovieDataModule {

    //5 minutes
    private static long CACHE_MAX_AGE = 5 * 60 * 100;

    @Provides
    @IntoSet
    TypeAdapterFactory provideTypeAdapterFactory() {
        return MovieTypeAdapterFactory.create();
    }

    @Provides
    @Singleton
    Store.MemoryStore<String, MovieDraft> provideCache(TimestampProvider timestampProvider){
        return new Cache<>(MovieDraft::movieId, timestampProvider);
    }

    @Provides
    @Singleton
    ReactiveStore<String, MovieDraft> provideReactiveStore(Store.MemoryStore<String, MovieDraft> cache){
        return new MemoryReactiveStore<>(MovieDraft::movieId, cache);
    }

}
