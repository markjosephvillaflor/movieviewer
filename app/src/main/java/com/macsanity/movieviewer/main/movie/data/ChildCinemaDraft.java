package com.macsanity.movieviewer.main.movie.data;

import android.support.annotation.NonNull;

import com.google.auto.value.AutoValue;
import com.google.gson.annotations.SerializedName;

/**
 * Created by macsanity on 2/8/18.
 */

@AutoValue
public abstract class ChildCinemaDraft {

    @NonNull
    @SerializedName("id")
    public abstract String id();

    @NonNull
    @SerializedName("cinema_id")
    public abstract String cinemaId();

    @NonNull
    @SerializedName("label")
    public abstract String label();

    @NonNull
    public static Builder builder(){
        return new AutoValue_ChildCinemaDraft.Builder();
    }

    @AutoValue.Builder
    interface Builder {

        Builder id(final String id);
        Builder cinemaId(final String cinema);
        Builder label(final String label);

        ChildCinemaDraft build();

    }

}
