package com.macsanity.movieviewer.main.movie.presentation.seatmap;

import dagger.Subcomponent;

/**
 * Created by macsanity on 2/7/18.
 */

@Subcomponent
public interface SeatComponent {

    interface SeatComponentCreator {
        SeatComponent seatComponent();
    }
}
