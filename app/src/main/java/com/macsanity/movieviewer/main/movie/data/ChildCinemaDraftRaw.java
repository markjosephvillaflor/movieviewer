package com.macsanity.movieviewer.main.movie.data;

import android.support.annotation.NonNull;

import com.google.auto.value.AutoValue;
import com.google.gson.Gson;
import com.google.gson.TypeAdapter;
import com.google.gson.annotations.SerializedName;

/**
 * Created by macsanity on 2/8/18.
 */

public class ChildCinemaDraftRaw {

    @NonNull
    @SerializedName("id")
    public String id;

    @NonNull
    @SerializedName("cinema_id")
    public String cinemaId;

    @NonNull
    @SerializedName("label")
    public String label;

}
