package com.macsanity.movieviewer.main.movie.presentation.seatmap;

import android.arch.lifecycle.ViewModelProvider;
import android.arch.lifecycle.ViewModelProviders;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.macsanity.base.injection.modules.ActivityModule;
import com.macsanity.base.presentation.BaseInjectingActivity;
import com.macsanity.movieviewer.main.CinemaAdapter;
import com.macsanity.movieviewer.main.DateAdapter;
import com.macsanity.movieviewer.R;
import com.macsanity.movieviewer.app.MovieViewerApplication;
import com.macsanity.movieviewer.main.TimeAdapter;
import com.macsanity.movieviewer.main.movie.SeatAdapter;
import com.macsanity.movieviewer.main.movie.data.AvailableSeatDraftRaw;
import com.macsanity.movieviewer.main.movie.data.ChildCinemaDraftRaw;
import com.macsanity.movieviewer.main.movie.data.CinemaDateDraft;
import com.macsanity.movieviewer.main.movie.data.CinemaDateDraftRaw;
import com.macsanity.movieviewer.main.movie.data.CinemaDraftRaw;
import com.macsanity.movieviewer.main.movie.data.CinemaListDraftRaw;
import com.macsanity.movieviewer.main.movie.data.CinemaTimeDraftRaw;
import com.macsanity.movieviewer.main.movie.data.SeatDraft;
import com.macsanity.movieviewer.main.movie.data.SeatSelectedDraft;
import com.macsanity.movieviewer.main.movie.data.SeatViewModel;
import com.macsanity.movieviewer.main.presentation.MainActivityComponent;

import java.sql.Time;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.inject.Inject;
import javax.inject.Named;

import by.anatoldeveloper.hallscheme.hall.HallScheme;
import by.anatoldeveloper.hallscheme.hall.MaxSeatsClickListener;
import by.anatoldeveloper.hallscheme.hall.ScenePosition;
import by.anatoldeveloper.hallscheme.hall.Seat;
import by.anatoldeveloper.hallscheme.hall.SeatListener;
import by.anatoldeveloper.hallscheme.view.ZoomableImageView;

public class SeatMapActivity extends BaseInjectingActivity<MainActivityComponent> implements SeatListener, MaxSeatsClickListener, AdapterView.OnItemSelectedListener {

    private ZoomableImageView imageViewChair;
    private TextView tvPrice;
    private TextView tvSeatName;

    @Inject
    @Named("SeatMapActivity")
    ViewModelProvider.Factory factory;

    private List<List<String>> item;
    private List<String> availableSeatDraftRawList;
    private List<String> seatSelectedDrafts;

    private  Spinner spinnerDate;
    private  Spinner spinnerCinema;
    private  Spinner spinnerTime;

    private DateAdapter mAdapterDate;
    private CinemaAdapter mAdapterCinema;
    private TimeAdapter mTimeAdapter;

    private SeatAdapter mSeatAdapter;

    private CinemaListDraftRaw draftRaw;
    private CinemaTimeDraftRaw cinemaTimeDraftRaw;
    private int seatPrice;

    private int count;

    @Override
    protected void onInject(@NonNull MainActivityComponent mainActivityComponent) {
        mainActivityComponent.inject(this);
    }

    @NonNull
    @Override
    protected MainActivityComponent createComponent() {
        MovieViewerApplication movieViewerApplication = MovieViewerApplication.class.cast(getApplication());
        ActivityModule activityModule = new ActivityModule(this);
        return movieViewerApplication.getComponent().createMainActivityComponent(activityModule);
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        item = new ArrayList<>();
        draftRaw = new CinemaListDraftRaw();
        cinemaTimeDraftRaw = new CinemaTimeDraftRaw();
        seatSelectedDrafts = new ArrayList<>();

        if(getIntent().getExtras() != null) {
            getSupportActionBar().setTitle("Theater " + getIntent().getExtras().getString("title"));
        }
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        final SeatViewModel seatViewModel = ViewModelProviders.of(this, factory).get(SeatViewModel.class);

        seatViewModel.getSeatLiveData().observe(this, this::setData);
        seatViewModel.getScheduleLiveData().observe(this, this::setScheduleData);

        imageViewChair = findViewById(R.id.image_seat);
        tvPrice = findViewById(R.id.tv_price);
        tvSeatName = findViewById(R.id.tv_seat_name);

        RecyclerView view = findViewById(R.id.rv_seats);

        spinnerDate = findViewById(R.id.spinner_date);
        spinnerCinema = findViewById(R.id.spinner_cinema);
        spinnerTime = findViewById(R.id.spinner_time);

        mAdapterDate = new DateAdapter(this);
        mAdapterCinema = new CinemaAdapter(this);
        mTimeAdapter = new TimeAdapter(this);
        mSeatAdapter = new SeatAdapter(this);

        view.setHasFixedSize(true);
        view.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
        view.setAdapter(mSeatAdapter);

        spinnerDate.setOnItemSelectedListener(this);
        spinnerCinema.setOnItemSelectedListener(this);
        spinnerTime.setOnItemSelectedListener(this);
    }

    private void setScheduleData(CinemaListDraftRaw cinemaListDraft) {
        draftRaw = cinemaListDraft;

        mAdapterDate.addData(draftRaw.dates);
        spinnerDate.setAdapter(mAdapterDate);

    }


    @Override
    protected int getLayoutId() {
        return R.layout.activity_seat_map;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                    finish();
                return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void setData(SeatDraft seatDraft) {
        List<List<String>> tempItem = seatDraft.seatmap();
        availableSeatDraftRawList = seatDraft.seatAvailable();
        for (int i = 0; i < tempItem.size(); i++) {
            List<String> c = tempItem.get(i);

            if (tempItem.get(i).size() != 38) {
                c.add(0, "");
                c.add("");
            }
        }

        item = tempItem;
    }

    private void initViews() {
        final HallScheme scheme = new HallScheme(imageViewChair, basicScheme(), imageViewChair.getContext());

        scheme.setScenePosition(ScenePosition.NORTH);
        scheme.setSeatListener(this);
        scheme.setMaxSelectedSeats(10);
        scheme.setMaxSeatsClickListener(this);
    }

    public Seat[][] basicScheme() {
        Seat seats[][] = new Seat[item.size()][item.get(0).size()];
        int k = 0;

        for (int i = 0; i < item.size(); i++) {
            List<String> c = item.get(i);

            for (int j = 0; j < c.size(); j++) {

                SeatView seat = new SeatView();
                seat.id = String.valueOf(c.get(j));
                seat.selectedSeatMarker = String.valueOf(c.get(j));

                if(availableSeatDraftRawList.contains(c.get(j))){
                    seat.status = HallScheme.SeatStatus.FREE;
                    seat.color = Color.GRAY;
                } else {
                    seat.status = HallScheme.SeatStatus.BUSY;
                }

                if (j == 0 || j == (item.get(0).size() - 1)) {
                    if (i < item.size()) {
                        seat.marker = String.valueOf((char) ('A' + i));
                        seat.status = HallScheme.SeatStatus.INFO;
                    }
                }

                if (c.get(j) instanceof String) {
                    if (c.get(j).contains("(")) {
                        seat.status = HallScheme.SeatStatus.EMPTY;
                    }
                }

                seats[i][j] = seat;
            }
        }

        return seats;
    }

    @Override
    public void selectSeat(String id) {

        count++;
        seatSelectedDrafts.add(id);
        appendText(seatSelectedDrafts);

    }

    @Override
    public void unSelectSeat(String id) {
        count--;
        seatSelectedDrafts.remove(id);
        appendText(seatSelectedDrafts);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int pos, long l) {

        Spinner spinner = (Spinner) parent;
        switch (spinner.getId()){

            case R.id.spinner_date:
                CinemaDraftRaw raws;

            for (int i = 0; i < draftRaw.cinemaDraftRaws.size(); i++) {
                    raws = draftRaw.cinemaDraftRaws.get(i);

                    if(draftRaw.dates.get(i).id().equals(draftRaw.cinemaDraftRaws.get(i).parent)){

                        mAdapterCinema.addData(raws.list);
                        mAdapterCinema.notifyDataSetChanged();
                    }
                }

                spinnerCinema.setAdapter(mAdapterCinema);
                break;

            case R.id.spinner_cinema:

                for (int i = 0; i < draftRaw.cinemaDraftRaws.size(); i++) {
                    raws = draftRaw.cinemaDraftRaws.get(i);

                    if(raws.list.get(i).id.equals(draftRaw.cinemaTimeDraftRaws.get(i).parent)){

                        if(raws.list.get(pos).id.equals(draftRaw.cinemaTimeDraftRaws.get(i).parent)){

                            mTimeAdapter.addData(draftRaw.cinemaTimeDraftRaws.get(i).times);
                            mTimeAdapter.notifyDataSetChanged();
                            imageViewChair.setEnabled(true);

                        } else {
                            Toast.makeText(this, "Sorry, No time slot available for this cinema.", Toast.LENGTH_SHORT).show();
                            mTimeAdapter.addData(Collections.emptyList());
                            mTimeAdapter.notifyDataSetChanged();
                            imageViewChair.setEnabled(false);

                        }
                    }
                }

                spinnerTime.setAdapter(mTimeAdapter);
                initViews();
                resetData();
                break;

            case R.id.spinner_time:

                for (int i = 0; i < draftRaw.cinemaTimeDraftRaws.size(); i++) {
                    cinemaTimeDraftRaw = draftRaw.cinemaTimeDraftRaws.get(i);
                }

                seatPrice = Integer.parseInt(cinemaTimeDraftRaw.times.get(pos).price);
                compute();
                break;
        }


    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {}

    @Override
    public void maxSeatsReached(String id) {
        Toast.makeText(this, "Sorry, You`re currently reached the limit.", Toast.LENGTH_SHORT).show();
    }

    private void compute(){
        int total = (seatSelectedDrafts.size() * seatPrice);
        tvPrice.setText(String.format("PHP %s", new DecimalFormat("###,##0.00").format(total)));
    }

    private void appendText(List<String> list){
        String price = TextUtils.join(" ", list);
        tvSeatName.setText(price);

        compute();
    }

    private void resetData(){
        tvPrice.setText("PHP 0.00");
        tvSeatName.setText("");
        seatSelectedDrafts.clear();
    }
}
