package com.macsanity.movieviewer.main.movie.data;

import android.support.annotation.NonNull;

import com.google.auto.value.AutoValue;
import com.google.gson.Gson;
import com.google.gson.TypeAdapter;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Mark Joseph Villaflor on 2/7/18.
 */

@AutoValue
public abstract class SeatDraftRaw {

    @SerializedName("seatmap")
    @NonNull
    abstract List<List<String>> seatmap();

    @SerializedName("available")
    @NonNull
    abstract AvailableSeatDraftRaw available();

    @NonNull
    public static TypeAdapter<SeatDraftRaw> typeAdapter(@NonNull final Gson gson) {
        return new AutoValue_SeatDraftRaw.GsonTypeAdapter(gson);
    }

    @NonNull
    public static Builder builder() {
        return new AutoValue_SeatDraftRaw.Builder();
    }

    @AutoValue.Builder
    interface Builder {

        Builder seatmap(final List<List<String>> seatmap);

        Builder available(final AvailableSeatDraftRaw seatDraftRaw);

        SeatDraftRaw build();
    }

}
