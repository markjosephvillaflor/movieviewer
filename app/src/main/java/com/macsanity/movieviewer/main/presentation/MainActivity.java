package com.macsanity.movieviewer.main.presentation;

import android.arch.lifecycle.ViewModelProvider;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.AppCompatTextView;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;

import com.macsanity.base.injection.modules.ActivityModule;
import com.macsanity.base.presentation.BaseInjectingActivity;
import com.macsanity.movieviewer.R;
import com.macsanity.movieviewer.app.MovieViewerApplication;
import com.macsanity.movieviewer.main.movie.data.MovieDraft;
import com.macsanity.movieviewer.main.movie.presentation.seatmap.SeatMapActivity;
import com.macsanity.movieviewer.main.movie.presentation.viewer.MovieViewModel;
import com.squareup.picasso.Picasso;

import javax.inject.Inject;
import javax.inject.Named;

public class MainActivity extends BaseInjectingActivity<MainActivityComponent> implements View.OnClickListener {

    @Inject
    @Named("MainActivity")
    ViewModelProvider.Factory factory;

    private AppCompatTextView mTxtTitle;
    private AppCompatTextView mTxtAdvisory;
    private AppCompatTextView mTxtRuntime;
    private AppCompatTextView mTxtGenre;
    private AppCompatTextView mTxtReleaseDate;
    private AppCompatTextView mTxtSypnosis;
    private AppCompatTextView mTxtCast;

    private AppCompatImageView mImgPoster;
    private AppCompatImageView mImgLandscapePoster;
    private String theater;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        final MovieViewModel movieViewModel = ViewModelProviders.of(this, factory).get(MovieViewModel.class);
        movieViewModel.getMovieLiveData().observe(this, this::setData);

        mTxtTitle = findViewById(R.id.text_movie_title);
        mTxtAdvisory = findViewById(R.id.text_advisory_rating);
        mTxtRuntime = findViewById(R.id.text_runtime);
        mTxtGenre = findViewById(R.id.text_genre);
        mTxtReleaseDate = findViewById(R.id.text_release_date);
        mTxtSypnosis = findViewById(R.id.text_synopsis);
        mTxtCast = findViewById(R.id.text_cast);

        mImgPoster = findViewById(R.id.image_poster_potrait);
        mImgLandscapePoster = findViewById(R.id.image_poster_landscape);

        AppCompatButton mBtnSeatMap = findViewById(R.id.button_view_seatmap);
        mBtnSeatMap.setOnClickListener(this);
    }


    @Override
    protected void onInject(@NonNull MainActivityComponent mainActivityComponent) {
        mainActivityComponent.inject(this);
    }

    @NonNull
    @Override
    protected MainActivityComponent createComponent() {
        MovieViewerApplication movieViewerApplication = MovieViewerApplication.class.cast(getApplication());
        ActivityModule activityModule = new ActivityModule(this);
        return movieViewerApplication.getComponent().createMainActivityComponent(activityModule);
    }

    @Override
    protected int getLayoutId() {
        return R.layout.activity_main;
    }

    private void setData(MovieDraft item) {
        Log.d("setData", item.poster());
        theater = item.theater();
        mTxtTitle.setText(item.canonicalTitle());
        mTxtAdvisory.setText(item.advisoryRating());
        mTxtRuntime.setText(item.runtimeMins());
        mTxtGenre.setText(item.genre());
        mTxtReleaseDate.setText(item.releaseDate());
        mTxtSypnosis.setText(item.synopsis());
        mTxtCast.setText(String.format("Cast: %s", TextUtils.join(",", item.cast())));

        Picasso.with(this).load(item.poster()).into(mImgPoster);
        Picasso.with(this).load(item.posterLandscape()).into(mImgLandscapePoster);

    }

    @Override
    public void onClick(View view) {
        Intent intent = new Intent(this,  SeatMapActivity.class);
        intent.putExtra("title", theater);
        startActivity(intent);
    }
}
