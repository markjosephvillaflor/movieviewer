package com.macsanity.movieviewer.main.movie.data;

import com.google.auto.value.AutoValue;

import java.util.List;

/**
 * Created by macsanity on 2/8/18.
 */

@AutoValue
public abstract class CinemaDraft {

    abstract String parent();

    abstract List<ChildCinemaDraft> list();

    public static Builder builder(){
        return new AutoValue_CinemaDraft.Builder();
    }

    @AutoValue.Builder
    interface Builder {

        Builder parent(final String parent);
        Builder list(final List<ChildCinemaDraft> childCinemaRaws);

        CinemaDraft build();
    }
}
