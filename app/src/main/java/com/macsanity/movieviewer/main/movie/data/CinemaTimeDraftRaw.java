package com.macsanity.movieviewer.main.movie.data;

import com.google.auto.value.AutoValue;
import com.google.gson.Gson;
import com.google.gson.TypeAdapter;
import com.google.gson.annotations.SerializedName;

import java.util.List;
import java.util.Map;

/**
 * Created by macsanity on 2/8/18.
 */


public class CinemaTimeDraftRaw {

    @SerializedName("parent")
    public String parent;

    @SerializedName("times")
    public List<ChildCinemaTimeDraftRaw> times;

}
