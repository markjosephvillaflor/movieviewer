package com.macsanity.movieviewer.main.movie.presentation.viewer;

import android.arch.lifecycle.ViewModelProvider;

import com.macsanity.base.presentation.utils.ViewModelUtil;
import com.macsanity.movieviewer.main.movie.data.SeatViewModel;

import javax.inject.Named;
import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * Created by Mark Joseph Villaflor on 2/6/18.
 */

@Module
public final class MovieViewModelModule {

    @Singleton
    @Provides
    @Named("MainActivity")
    static ViewModelProvider.Factory provideMovieViewModelProviderFactory(ViewModelUtil viewModelUtil, MovieViewModel movieViewModel){
        return viewModelUtil.createFor(movieViewModel);
    }

    @Singleton
    @Provides
    @Named("SeatMapActivity")
    static ViewModelProvider.Factory provideSeatViewModelProviderFactory(ViewModelUtil viewModelUtil, SeatViewModel seatViewModel) {
        return viewModelUtil.createFor(seatViewModel);
    }
}
