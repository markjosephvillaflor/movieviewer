package com.macsanity.movieviewer.app;

import com.macsanity.base.injection.modules.ActivityModule;
import com.macsanity.movieviewer.main.presentation.MainActivityComponent;

import javax.inject.Singleton;

import dagger.BindsInstance;
import dagger.Component;

/**
 * Created by Mark Joseph Villaflor on 2/6/18.
 */

@Singleton
@Component(modules = {
        ApplicationModule.class,
        NetworkModule.class,
        DataModule.class,
        ViewModelModule.class
        })
public interface ApplicationComponent {

    void inject(MovieViewerApplication movieViewerApplication);

    MainActivityComponent createMainActivityComponent(ActivityModule activityModule);

    @Component.Builder
    interface Builder {

        @BindsInstance
        Builder application(MovieViewerApplication application);

        ApplicationComponent build();
    }
}
