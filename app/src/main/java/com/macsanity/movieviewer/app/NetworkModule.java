package com.macsanity.movieviewer.app;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.TypeAdapterFactory;
import com.macsanity.base.BuildConfig;
import com.macsanity.movieviewer.main.movie.data.MovieService;


import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import javax.inject.Named;
import javax.inject.Qualifier;
import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by Mark Joseph Villaflor on 2/6/18.
 */

@Module
final class NetworkModule {

    private static final String API_URL = "API_URL";


    @Provides
    @Singleton
    static OkHttpClient provideApiOkHttpClient() {
        OkHttpClient.Builder okBuilder = new OkHttpClient.Builder();

        okBuilder.connectTimeout(15, TimeUnit.SECONDS)
                .readTimeout(1, TimeUnit.MINUTES)
                .writeTimeout(5, TimeUnit.MINUTES);

        okBuilder.interceptors().add(chain -> {

            Request orig = chain.request();

            Request.Builder reqBuilder = orig.newBuilder()
                    .header("Accept", "application/json")
                    .header("Cache-Control", "private, must-revalidate")
                    .method(orig.method(), orig.body());

            return chain.proceed(reqBuilder.build());
        });

        return okBuilder.build();
    }

    @Provides
    @Singleton
    static Retrofit provideMovieApi(@Named(API_URL) String baseUrl, Gson gson, OkHttpClient client) {

        OkHttpClient.Builder httpClientBuilder = client.newBuilder();

        if (BuildConfig.DEBUG) {
            HttpLoggingInterceptor loggingInterceptor = new HttpLoggingInterceptor();
            loggingInterceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
            httpClientBuilder.addInterceptor(loggingInterceptor);
        }

        return new Retrofit.Builder()
                .baseUrl(baseUrl)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .callFactory(httpClientBuilder.build())
                .build();
    }

    @Provides
    @Named(API_URL)
    static String provideBaseUrl() {
        return BuildConfig.IMAGE_VIEWER_API_URL;
    }


    @Provides
    @Singleton
    MovieService movieService(Retrofit retrofit) {
        return retrofit.create(MovieService.class);
    }

    @Provides
    @Singleton
    static Gson provideGson(Set<TypeAdapterFactory> typeAdapters) {
        final GsonBuilder builder = new GsonBuilder();

        for (TypeAdapterFactory factory : typeAdapters) {
            builder.registerTypeAdapterFactory(factory);
        }

        return builder.create();
    }


}
