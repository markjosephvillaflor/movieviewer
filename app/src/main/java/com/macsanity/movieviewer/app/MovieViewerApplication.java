package com.macsanity.movieviewer.app;

import android.app.Activity;
import android.app.Application;
import android.support.annotation.CallSuper;

import javax.inject.Inject;

import dagger.android.AndroidInjector;
import dagger.android.DaggerApplication;
import dagger.android.DispatchingAndroidInjector;
import dagger.android.HasActivityInjector;

/**
 * Created by Mark Joseph Villaflor on 2/6/18.
 */

public class MovieViewerApplication extends Application {

    private ApplicationComponent applicationComponent;

    @Override
    public void onCreate() {
        super.onCreate();
        getComponent().inject(this);
    }

    public ApplicationComponent getComponent(){
        if(applicationComponent == null) {
           applicationComponent = DaggerApplicationComponent.builder().application(this).build();
        }
        return applicationComponent;
    }
}
