package com.macsanity.movieviewer.app;

import android.app.Application;
import android.content.Context;

import com.macsanity.base.injection.qualifiers.ForApplication;

import dagger.Binds;
import dagger.Module;
import dagger.Provides;

/**
 * Created by Mark Joseph Villaflor on 2/6/18.
 */
@Module
public class ApplicationModule {

    @ForApplication
    @Provides
     Context provideContext(Application application){
        return application.getApplicationContext();
    }
}
