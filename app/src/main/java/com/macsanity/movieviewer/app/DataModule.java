package com.macsanity.movieviewer.app;

import com.macsanity.movieviewer.main.movie.data.CinemaDataModule;
import com.macsanity.movieviewer.main.movie.data.MovieDataModule;
import com.macsanity.movieviewer.main.movie.data.SeatDataModule;

import dagger.Module;

/**
 * Created by Mark Joseph Villaflor on 2/6/18.
 */

@Module(includes = {MovieDataModule.class, SeatDataModule.class, CinemaDataModule.class})
final class DataModule {
}
