package com.macsanity.movieviewer.app;

import com.macsanity.movieviewer.main.movie.presentation.viewer.MovieViewModelModule;

import dagger.Module;

/**
 * Created by macsanity on 2/6/18.
 */

@Module(includes = { MovieViewModelModule.class})
public final class ViewModelModule {}
